/// Single pixel without alpha
#[derive(Copy, Clone, Debug, PartialEq, Eq)]
pub struct Rgb
{
    pub r: u8,
    pub g: u8,
    pub b: u8,
}

/// Single pixel with alpha
#[derive(Copy, Clone, Debug, PartialEq, Eq)]
pub struct Rgba
{
    pub r: u8,
    pub g: u8,
    pub b: u8,
    pub a: u8,
}

/// Number of channels
///
/// This stores whether RGB or RGBA is used.
#[derive(Copy, Clone, Debug, PartialEq, Eq)]
pub enum Channel
{
    Rgb,
    Rgba,
}

// The "sRGB" in the second paragraph shouldn't be in backticks.
#[allow(clippy::doc_markdown)]
/// Colorspace of the image
///
/// This stores whether all channels are linear
/// ([`Colorspace::Linear`]) or if it's sRGB and only the alpha
/// channel (if it exists) is linear ([`Colorspace::Srgb`]).
#[derive(Copy, Clone, Debug, PartialEq, Eq)]
pub enum Colorspace
{
    Srgb,
    Linear,
}

impl Rgb
{
    /// Creates a new [`Rgb`]
    ///
    /// This creates a new [`Rgb`]. Please notice that you can also do
    /// this manual with `Rgb {r: …, g: …, b: …}`.
    #[must_use]
    pub const fn new(r: u8, g: u8, b: u8) -> Self
    {
        Self { r, g, b }
    }
}

impl Rgba
{
    /// Creates a new [`Rgba`]
    ///
    /// This creates a new [`Rgba`]. Please notice that you can also
    /// do this manual with `Rgb {r: …, g: …, b: …, a: …}`.
    #[must_use]
    pub const fn new(r: u8, g: u8, b: u8, a: u8) -> Self
    {
        Self { r, g, b, a }
    }
}

#[must_use]
pub const fn hash(color: Rgba) -> usize
{
    let r = color.r as usize;
    let g = color.g as usize;
    let b = color.b as usize;
    let a = color.a as usize;

    (r * 3 + g * 5 + b * 7 + a * 11) % 64
}
