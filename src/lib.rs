/*
    serialqoi – Serial QOI de/encoder
    Copyright (C) 2022  Matthias Kaak

    This program is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program.  If not, see <https://www.gnu.org/licenses/>.
*/

#![warn(
    clippy::all,
    clippy::pedantic,
    clippy::nursery,
    clippy::cargo_common_metadata
)]
// Anachronism
#![allow(clippy::non_ascii_literal)]
// More or less manual checked and documentation agrees with me that
// it's usually not needed.
#![allow(clippy::cast_possible_truncation, clippy::cast_lossless)]

//! # Serial QOI de/encoder
//!
//! [QOI](https://qoiformat.org/) is a incredibly fast, simple,
//! lossless image compression format.  This library is a serial
//! implementation of it, based on [`Read`](std::io::Read) and
//! [`Write`](std::io::Write).  Due to this it's generic over anything
//! that implements this, e.g. [`File`](std::fs::File) and
//! [`Vec<u8>`](std::vec::Vec). [`QoiReader`] decodes an image and
//! [`QoiWriter`] encodes an image.
//!
//! ## Contributing
//! As everything `serialqoi` too always can be improved.  If you want
//! to help (like with a feature (request), a bug report or
//! documentation improvements), **please help**.
//!
//! I assume that unless stated otherwise every contribution follows
//! the necessary license.
//!
//! ## License
//! `serialqoi` is released under the GNU General Public License
//! version 3 or (at your option) any later version.
//!
//! For more see
//! [LICENSE.md](https://github.com/zvavybir/serialqoi/blob/master/LICENSE.md).

pub(crate) mod common;
mod qoireader;
mod qoiwriter;
#[cfg(test)]
mod tests;

pub use common::{Channel, Colorspace, Rgb, Rgba};
pub use qoireader::QoiReader;
pub use qoiwriter::QoiWriter;
