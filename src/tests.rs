use crate::{Channel, Colorspace, QoiReader, QoiWriter, Rgb, Rgba};

// This must be allowed for many tests to be possible.
#[allow(clippy::too_many_lines)]
#[test]
fn write_read_roundtrip()
{
    let tests = [
        (
            (0, 0, vec![]),
            vec![
                b'q', b'o', b'i', b'f', 0, 0, 0, 0, 0, 0, 0, 0, 3, 0, 0, 0, 0, 0, 0, 0, 0, 1,
            ],
        ),
        (
            (1, 1, vec![Rgb::new(50, 100, 200)]),
            vec![
                b'q', b'o', b'i', b'f', 0, 0, 0, 1, 0, 0, 0, 1, 3, 0, 0xfe, 50, 100, 200, 0, 0, 0,
                0, 0, 0, 0, 1,
            ],
        ),
        (
            (
                1,
                3,
                vec![
                    Rgb::new(50, 100, 200),
                    Rgb::new(50, 100, 200),
                    Rgb::new(50, 100, 200),
                ],
            ),
            vec![
                b'q',
                b'o',
                b'i',
                b'f',
                0,
                0,
                0,
                1,
                0,
                0,
                0,
                3,
                3,
                0,
                0xfe,
                50,
                100,
                200,
                0b1100_0000 + 1,
                0,
                0,
                0,
                0,
                0,
                0,
                0,
                1,
            ],
        ),
        (
            (
                5,
                1,
                vec![
                    Rgb::new(50, 100, 200),
                    Rgb::new(24, 69, 167),
                    Rgb::new(23, 67, 168),
                    Rgb::new(50, 100, 200),
                    Rgb::new(50, 100, 200),
                ],
            ),
            vec![
                b'q',
                b'o',
                b'i',
                b'f',
                0,
                0,
                0,
                5,
                0,
                0,
                0,
                1,
                3,
                0,
                0xfe,
                50,
                100,
                200,
                0b1000_0000 + 1,
                13 * 16 + 6,
                0b0100_0000 + 16 + 3,
                55,
                0b1100_0000,
                0,
                0,
                0,
                0,
                0,
                0,
                0,
                1,
            ],
        ),
    ];

    for ((width, height, pixels), correct_output) in tests
    {
        let mut output = vec![];
        let mut writer = QoiWriter::new(&mut output, width, height, Channel::Rgb, Colorspace::Srgb)
            .expect("Shouldn't fail");

        pixels
            .iter()
            .for_each(|p| writer.write_rgb(*p).expect("Shouldn't fail"));

        writer.close().expect("Shouldn't fail");

        assert_eq!(output, correct_output);

        let output = QoiReader::new(&output[..]).expect("Shouldn't fail");
        assert_eq!(output.width(), width);
        assert_eq!(output.height(), height);
        assert_eq!(output.channels(), Channel::Rgb);
        assert_eq!(output.colorspace(), Colorspace::Srgb);
        assert_eq!(
            output
                .map(|p| p.expect("Shouldn't fail"))
                .map(|Rgba { r, g, b, a: _ }| Rgb { r, g, b })
                .collect::<Vec<_>>(),
            pixels
        );
    }
}
