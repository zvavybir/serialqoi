# Serial QOI de/encoder

[QOI](https://qoiformat.org/) is a incredibly fast, simple,
lossless image compression format.  This library is a serial
implementation of it, based on [`Read`](std::io::Read) and
[`Write`](std::io::Write).  Due to this it's generic over anything
that implements this, e.g. [`File`](std::fs::File) and
[`Vec<u8>`](std::vec::Vec). `QoiReader` decodes an image and
`QoiWriter` encodes an image.

### Contributing
As everything `serialqoi` too always can be improved.  If you want
to help (like with a feature (request), a bug report or
documentation improvements), **please help**.

I assume that unless stated otherwise every contribution follows
the necessary license.

### License
`serialqoi` is released under the GNU General Public License
version 3 or (at your option) any later version.

For more see
[LICENSE.md](https://github.com/zvavybir/serialqoi/blob/master/LICENSE.md).
